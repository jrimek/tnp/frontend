# Readme

This is the frontend part of the new version of TNP, meaning this is what the user/visitor will actually see.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# deploy to aws, custom script, see package.json
npm run deploy
```

