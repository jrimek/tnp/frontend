import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
Vue.use(vueResource)

export default new Router({
  mode: 'history',
  routes: [
     {
      path: '/',
      name: 'FrontPage',
      component: FrontPage
    },
      {
      path: '/calender',
      name: 'CalenderPage',
      component: CalenderPage
    },
      {
      path: '/about',
      name: 'AboutPage',
      component: AboutPage
    },
      {
      path: '/impressum',
      name: 'ImpressumPage',
      component: ImpressumPage
    },

    {
      path: '/category/:name',
      name: 'CategoryPage',
      component: CategoryPage
    },
    {
        path: '/galery-categories',
        name: 'GaleryCategoriesPage',
        component: GaleryCategoriesPage
    }

  ]
})
