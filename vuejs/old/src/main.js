// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App'
import router from './router'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import FrontPage from './components/FrontPage'
import CategoryPage from '../components/CategoryPage'
import GaleryCategoriesPage from '../components/GaleryCategoriesPage'
import CalenderPage from '../components/CalenderCategoriesPage'
import AboutPage from '../components/AboutPage'
import ImpressumPage from '../components/ImpressumPage'

import vueResource from 'vue-resource'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
